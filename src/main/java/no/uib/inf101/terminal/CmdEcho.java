package no.uib.inf101.terminal;

class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        // TODO Auto-generated method stub
        String s = "";
        for (String i : args) {
            s += i + " ";
        }
        return s;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "echo";
    }
    
}
